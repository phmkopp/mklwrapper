if( ( DEFINED MKL_DIR ) AND ( EXISTS ${MKL_DIR} ) )
    set( MKL_ROOT_DIR ${MKL_DIR} )
endif( ( DEFINED MKL_DIR ) AND ( EXISTS ${MKL_DIR} ) )

# Find mkl root in environment variable
if( ( NOT DEFINED MKL_ROOT_DIR ) OR ( NOT EXISTS ${MKL_ROOT_DIR} ) ) 
        
    message( STATUS "Trying environment variable" )

    if( DEFINED ENV{MKL_ROOT} )
       message( STATUS "Setting from environment variable" )
       set( MKL_ROOT_DIR $ENV{MKL_ROOT} )
    endif( DEFINED ENV{MKL_ROOT} )

    if( DEFINED ENV{MKLROOT} )
       message( STATUS "Setting from environment variable" )
       set( MKL_ROOT_DIR $ENV{MKLROOT} )
    endif( DEFINED ENV{MKLROOT} )
        
endif( ( NOT DEFINED MKL_ROOT_DIR ) OR ( NOT EXISTS ${MKL_ROOT_DIR} ) )

# Otherwise try manual paths
if( ( NOT DEFINED MKL_ROOT_DIR ) OR ( NOT EXISTS ${MKL_ROOT_DIR} ) ) 

        if( WIN32 )
            set( MKL_ROOT_DIR "C:/Program Files (x86)/IntelSWTools/compilers_and_libraries/windows/mkl" )
        else( WIN32 )
            set( MKL_ROOT_DIR "/opt/intel/mkl" )
        endif( WIN32 )
    
endif( ( NOT DEFINED MKL_ROOT_DIR ) OR ( NOT EXISTS ${MKL_ROOT_DIR} ) ) 

# Setup paths
if( EXISTS ${MKL_ROOT_DIR} )

    set( MKL_FOUND TRUE )
    
    message( STATUS "MKL was found at " ${MKL_ROOT_DIR} )
         
    set( MKL_INCLUDE_DIR "${MKL_ROOT_DIR}/include" )
    
    if ( WIN32 )
            
        set( INTEL_MKL_LIB_DIR ${MKL_ROOT_DIR}/lib/intel64_win )
        set( INTEL_COMPILER_LIB_DIR ${MKL_ROOT_DIR}/../compiler/lib/intel64_win )

        # Static libraries mklvars.bat doesn't need to be executed
        set( MKL_LIBRARIES ${INTEL_MKL_LIB_DIR}/mkl_intel_ilp64.lib 
                           ${INTEL_MKL_LIB_DIR}/mkl_intel_thread.lib 
                           ${INTEL_MKL_LIB_DIR}/mkl_core.lib 
                           ${INTEL_COMPILER_LIB_DIR}/libiomp5md.lib )
        
    else ( WIN32 )
        
        set( MKL_LIBRARIES "-L${MKL_ROOT_DIR}/lib/intel64 -lmkl_intel_ilp64 -lgomp -lmkl_gnu_thread -lmkl_core" )

        link_directories( ${MKL_ROOT_DIR}/lib/intel64 )

    endif ( WIN32 )
else( EXISTS ${MKL_ROOT_DIR} )
    set( MKL_ROOT_DIR "Not found" )
    set( MKL_FOUND FALSE )
endif( EXISTS ${MKL_ROOT_DIR} )

if( NOT EXISTS ${MKL_ROOT_DIR} )
    set( MKL_DIR ${MKL_ROOT_DIR} CACHE PATH "Mkl root dir with subfolders lib and include." FORCE )
    MESSAGE(FATAL_ERROR "Could not find MKL libraries")
endif( NOT EXISTS ${MKL_ROOT_DIR} )


