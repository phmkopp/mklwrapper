#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

#include "mklwrapper/mklwrapper.hpp"

namespace mklwrapper
{

TEST_CASE( "pardiso_test" )
{
    constexpr size_t nnz = 12;

    std::uint64_t* indices = new std::uint64_t[nnz] { 0, 1, 0, 2, 4, 1, 2, 3, 2, 1, 2, 4 };
    std::uint64_t* indptr = new std::uint64_t[6]  { 0, 2, 5, 8, 9, 12 };
    double* data = new double[nnz] { 2.0, 3.0, 3.0, 4.0, 6.0, -1.0, -3.0, 2.0, 1.0, 4.0, 2.0, 1.0 };

    std::vector<double> rhs = { 8.0, 45.0, -3.0, 3.0, 19.0 };
    std::vector<double> solution( rhs.size( ), 4.3 );

    mklwrapper::pardiso( indptr, indices, data, rhs.data( ), 5, false, solution.data( ) );

    std::vector<double> expectedSolution = { 1.0, 2.0, 3.0, 4.0, 5.0 };

    REQUIRE( solution.size( ) == 5 );

    for( size_t i = 0; i < 5; ++i )
    {
        CHECK( solution[i] == doctest::Approx( expectedSolution[i] ).epsilon( 1e-12 ) );
    }
}

} // mklwrapper
