#ifndef MKLWRAPPER_MKLWRAPPER_HPP
#define MKLWRAPPER_MKLWRAPPER_HPP

#include <functional>

namespace mklwrapper
{

using mklwrapper_int = long long;

enum class MatrixProperty
{
    Symmetric, Unsymmetric
};

using SparseSolver = std::function<void( const mklwrapper_int* indptr,
                                         const mklwrapper_int* indices,
                                         const double* values,
                                         mklwrapper_int numberOfRows,
                                         bool symmetric,
                                         double* target )>;

enum class PardisoPhase : mklwrapper_int { REORDER = 11, FACTORIZATION = 22, SOLUTION = 33, CLEANUP = -1 };

void pardiso( const std::size_t* indptr,
              const std::size_t* indices,
              const double* matrixValues,
              const double* rhsValues,
              std::size_t numberOfRows,
              bool symmetric,
              double* solutionTarget );

// Will change sparse format to 1-based
//LinearOperator iluPreconditioner( const UnsymmetricSparseMatrix& matrix );

} // namespace mklwrapper

#endif // MKLWRAPPER_MKLWRAPPER_HPP
