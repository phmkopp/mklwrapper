#ifndef MKL_ILP64
#define MKL_ILP64
#endif

//#include <mkl.h>
#include <mkl_pardiso.h>

#include <functional>
#include <vector>
#include <map>
#include <string>
#include <iostream>

#include "mklwrapper/mklwrapper.hpp"

namespace mklwrapper
{

static const std::map<PardisoPhase, std::string> phaseToString
{
    { PardisoPhase::REORDER      , "reorder"       },
    { PardisoPhase::FACTORIZATION, "factorization" },
    { PardisoPhase::SOLUTION     , "solution"      },
    { PardisoPhase::CLEANUP      , "cleanup"       }
};

void pardiso( const std::size_t* indptr,
              const std::size_t* indices,
              const double* matrixValues,
              const double* rhsValues,
              std::size_t numberOfRows,
              bool symmetric,
              double* solutionTarget )
{
    if( !numberOfRows )
    {
        return;
    }

    void* internalSolverMemoryPointer[64];

    static_assert( sizeof( mklwrapper_int ) == sizeof( std::uint64_t ), "Incompatible sizes." );

    mklwrapper_int iparm[64];

    mklwrapper_int maximuNumberOfFactorization = 1;
    mklwrapper_int matrixNumber = 1;
    mklwrapper_int matrixType = 11;
    mklwrapper_int numberOfRightHandSides = 1;
    mklwrapper_int n = static_cast<mklwrapper_int>( numberOfRows );
    mklwrapper_int error = 0;
    mklwrapper_int integerDummy = 0;
    mklwrapper_int messageLevel = 0;

    std::fill( internalSolverMemoryPointer, internalSolverMemoryPointer + 64, nullptr );
    std::fill( iparm, iparm + 64, 0 );

    PARDISOINIT( internalSolverMemoryPointer, &matrixType, iparm );

    iparm[1] = 3;  // fill in reducing ordering: 3-> parallel version of metis
    //iparm[9] = 8; // Pivoting perturbation : 10 ^ -8
    iparm[23] = 1; // Parallel factorization control: 1->improved two-level factorization algorithm
    iparm[26] = 1; // Enable matrix consistency checks
    iparm[34] = 1; // One- or zero-based indexing of columns and rows: 1->zero based

    auto callPardiso = [&]( PardisoPhase phase,
                            double* data,
                            mklwrapper_int* indices,
                            mklwrapper_int* indptr,
                            double* right,
                            double* solution )
    {
        mklwrapper_int phaseInt = static_cast<mklwrapper_int>( phase );

        PARDISO_64( internalSolverMemoryPointer, &maximuNumberOfFactorization, &matrixNumber, &matrixType, &phaseInt, &n, data,
                    indptr, indices, &integerDummy, &numberOfRightHandSides, iparm, &messageLevel, right, solution, &error );

        if( error != 0 )
        {
            std::cout << "Error during " + phaseToString.at( phase ) + " phase: " + std::to_string( error ) << std::endl;
        }
    };

    mklwrapper_int* i = const_cast<mklwrapper_int*>( reinterpret_cast<const mklwrapper_int*>( indices ) );
    mklwrapper_int* j = const_cast<mklwrapper_int*>( reinterpret_cast<const mklwrapper_int*>( indptr ) );
    double* v = const_cast<double*>( matrixValues );
    double* f = const_cast<double*>( rhsValues );

    callPardiso( PardisoPhase::REORDER, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::FACTORIZATION, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::SOLUTION, v, i, j, f, solutionTarget );
    callPardiso( PardisoPhase::CLEANUP, nullptr, nullptr, nullptr, nullptr, nullptr );
}


//LinearOperator iluPreconditioner( const UnsymmetricSparseMatrix& matrix )
//{
//    throw 0;
//    //return;
//}

} // mklwrapper